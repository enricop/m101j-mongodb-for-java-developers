use kappa;
db.zippi.aggregate([ 
            {"$match":{state:{"$in": ['CA','NY']}}},
	    {"$group":{"_id":{state:"$state", city:"$city"}, "pop1":{"$sum":"$pop"}}},
	    {"$match":{pop1:{"$gt":25000}}},
	    {"$group": {_id: null, avg1: {"$avg": "$pop1"}}}
]);
