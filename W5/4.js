use gradee;
db.list.aggregate([
	{"$match":{"scores.type":{$in:["exam","homework"]}}},
	{"$project":{student_id:1, class_id:1, marks:"$scores.score"}},
       	{"$unwind":"$marks"},
	{"$group":{"_id":{sid:"$student_id",cid:"$class_id"}, avg1:{"$avg":"$marks"}}}, 
	{"$group":{"_id":"$_id.cid",avg2:{"$avg":"$avg1"}}}
]);
